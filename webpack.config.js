const ExtractTextPlugin = require('extract-text-webpack-plugin')
const webpack = require('webpack')

const entry = {
  'index.js': [
    'webpack-hot-middleware/client',
    'react-hot-loader/patch',
    'babel-polyfill',
    './src/app/server/init.js'
  ]
}

const extractCssLoader = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use: [
    { loader: 'css-loader', options: { importLoaders: 1 } },
    'postcss-loader',
    { loader: 'sass-loader' }
  ]
})

const rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: 'babel-loader'
  },
  {
    test: /\.(css|scss)$/,
    exclude: /node_modules/,
    use: ['css-hot-loader'].concat(extractCssLoader)
  }
]

const plugins = [
  new ExtractTextPlugin({
    filename: 'styles.css',
    allChunks: true
  }),
  new webpack.HotModuleReplacementPlugin()
]

module.exports = {
  entry,
  output: {
    path: __dirname,
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    loaders: rules
  },
  plugins
}
