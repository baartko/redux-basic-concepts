import React from 'react'
import createApplication from './app/base/create-application'
import createStore from './app/store/create-store'

// Load UI
import App from './app/providers/store-provider'

// Set up effects & reducers
import * as logic from './logic'

// Load styles & icons
import './styles/main.scss'

function render (app) {
  return <App app={app} />
}

import { init, redirect } from './logic/app/actions'

function prepare (url, data) {
  // Initialize store
  const store = createStore(logic.reducers, logic.effects, data)

  const app = createApplication(store, render)

  // Make application instance available for effects
  store.shared.app = app

  const state = store.getState()

  app.store.dispatch(init(store.getState().app.url || url))

  if (state.app.url !== url) {
    app.store.dispatch(redirect(url))
  }

  return app
}

export default prepare
