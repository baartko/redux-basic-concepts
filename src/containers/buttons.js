import { connect } from 'react-redux'
import Buttons from '../components/buttons'
import get from 'lodash/get'

import { onButtonClick } from '../logic/app/actions'

const mapStateToProps = state => ({
  previousUrl: get(state, 'app.previousUrl', '')
})

const mapDispatchToProps = dispatch => ({
  onButton1Click: () => dispatch(onButtonClick('btn1')),
  onButton2Click: () => dispatch(onButtonClick('btn2'))
})

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)
