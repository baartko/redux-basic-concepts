import { compose, createStore, applyMiddleware } from 'redux'
import createEffectsMiddleware from './create-effects'
import createLoggerMiddleware from './create-logger'
import combineReducers from './combine-reducers'
import { composeWithDevTools } from 'redux-devtools-extension'

function finalCreateStore (reducers, effects, data = {}) {
  // We need to be sure that functions are not reused
  reducers = Object.assign({}, reducers)
  effects = Object.assign({}, effects)

  // Make wrapper for all effects to make usage easier
  for (const name of Object.keys(effects)) {
    const effect = effects[name]

    effects[name] = function (action, dispatch, state, shared) {
      return effect(action, {
        ...shared,
        dispatch,
        state
      })
    }
  }

  // assign share data to effects
  const shared = {
    a: 123
  }

  // Set up simple effects middleware
  const effectsMiddleware = createEffectsMiddleware(effects, shared)
  const loggerMiddleware = createLoggerMiddleware()

  const middlewares = composeWithDevTools(
      applyMiddleware(
      effectsMiddleware,
      loggerMiddleware
    )
  )

  const store = createStore(
    combineReducers(reducers),
    data,
    compose(middlewares)
  )

  store.shared = shared

  return store
}

export default finalCreateStore
