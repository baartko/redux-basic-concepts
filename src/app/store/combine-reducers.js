import { combineReducers } from 'redux'

/**
 * Make switch/case function to prepare reducer per action.
 *
 * @param {object} reducers
 */
const handleReducers = reducers => (state, action) => {
  state = state || {}

  if (reducers[action.type]) {
    return reducers[action.type](state, action)
  }

  return state
}

/**
 * Prepare final version of combined reducers
 * @param {object} reducers
 */
const finalCombineReducers = reducers => {
  // handle reducers on the fly to make usage easier
  for (let key of Object.keys(reducers)) {
    reducers[key] = handleReducers(reducers[key])
  }

  return combineReducers(reducers)
}

export default finalCombineReducers
