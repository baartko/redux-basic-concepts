function createMiddleware (effects, shared = null) {
    effects = { ...effects }
  
    function effectsMiddleware ({ dispatch, getState }) {
      return next => action => {
        if (effects[action.type]) {
          setTimeout(() => {
            [].concat(effects[action.type]).forEach(x => {
              console.log('effects:', action.type)
              x({ ...action }, dispatch, getState(), shared)
            })
          }, 1)
        }
  
        next(action)
      }
    }
  
    return effectsMiddleware
  }
  
export default createMiddleware
  