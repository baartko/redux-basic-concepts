function createLogger () {
  function loggerMiddleware () {
    return next => action => {
      console.log(`action: ${action.type}`);
      next(action)
    }
  }

  return loggerMiddleware
}  
  
export default createLogger
  