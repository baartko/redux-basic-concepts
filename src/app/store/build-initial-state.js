/**
 * Build initial state which should be used
 */
const buildInitialState = () => ({
  app: {
    title: 'React Redux boilerplate',
  }
})

export default buildInitialState
