/**
 * Application instance, to store data per user session
 */
class Application {
    /**
     * @param {object} router
     * @param {object} api
     */
    constructor (store,vdom) {
      this.store = store
      this.vdom = vdom.bind(null, this)
    }
  }
  
  export default Application
  