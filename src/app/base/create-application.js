import Application from './application'

/**
 * Factory for application instance
 * It's separated to allow any abstraction to put there
 */
const createApplication = (store, vdom) => {
  return new Application(
    store,
    vdom,
  )
}

export default createApplication
