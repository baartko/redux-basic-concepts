import { Provider as ReduxProvider } from 'react-redux'
import App from '../../pages'

function StoreProvider ({ app }) {
  return (
    <ReduxProvider store={app.store}>
      <App />
    </ReduxProvider>
  )
}

export default StoreProvider
