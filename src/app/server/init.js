import { render } from 'react-dom'
import prepareApplication from '../../prepare-application'
import buildInitialState from '../store/build-initial-state';

const getLocation = () => location.pathname + (location.search || '')
const app = prepareApplication(getLocation(), buildInitialState())

function vdom () {
  const { AppContainer } = require('react-hot-loader')
  return <AppContainer>{ app.vdom() }</AppContainer>
}

render(
  vdom(),
  document.getElementById('root')
)
