const http = require('http')
const path = require('path')

const express = require('express')
const app = express()

var webpack = require('webpack')
var webpackConfig = require(process.env.WEBPACK_CONFIG ? process.env.WEBPACK_CONFIG : '../../../webpack.config')
var compiler = webpack(webpackConfig)

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true, publicPath: webpackConfig.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler, {
  log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
}))

app.get('/', (req, res) => res.sendFile(path.join(__dirname, '/index.html')))
app.use('/', express.static(path.join(__dirname, '../../../dist')))

if (require.main === module) {
  const server = http.createServer(app)
  server.listen(process.env.PORT || 5000, () => console.log('Listening on %j', server.address()))
}
