import * as app from './app'
import * as meta from './meta'

function combineEffects (...effects) {
  const result = {}

  // Go through all the effects and get the actions
  for (const e of Object.keys(effects)) {
    for (const action of Object.keys(effects[e])) {
      if (!result[action]) {
        result[action] = []
      }

      result[action].push(effects[e][action])
    }
  }

  // Build proxy functions for these actions
  for (const action of Object.keys(result)) {
    const calls = result[action]

    result[action] = (...args) => Promise.all(calls.map(f => f(...args)))
  }

  return result
}

// Load reducers
export const reducers = {
  app: app.reducers,
  meta: meta.reducers
}

// Load effects
export const effects = combineEffects(
  app.effects,
  meta.effects
)
