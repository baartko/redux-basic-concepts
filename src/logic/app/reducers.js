export const APP_REDIRECT = (state, { url }) => {
  console.log('reducers:', 'APP_REDIRECT')
  return {
    ...state,
    url,
    previousUrl: state.url,
    modal: {
      active: null,
      params: {}
    }
  }
}

export const APP_INIT = (state, { url }) => {
  console.log('reducers:', 'APP_INIT')
  return {
    ...state,
    url,
    previousUrl: state.url,
    modal: {
      active: null,
      params: {}
    }
  }
}

export const APP_BUTTON_CLICK = (state, { clicked }) => {
  console.log('reducers', 'APP_BUTTON_CLICK')
  return {
    ...state,
    clicked
  }
}
