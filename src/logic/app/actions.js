export const init = (url) => ({
    type: 'APP_INIT',
    url
  })
  
export const redirect = url => ({
  type: 'APP_REDIRECT',
  url
})

export const onButtonClick = name => ({
  type: 'APP_BUTTON_CLICK',
  clicked: name
})
