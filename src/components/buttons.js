import React from 'react'

class Buttons extends React.Component {
  // onButton1Click() {
  //   console.log('button1 clicked')
  // }
  
  onButton2Click() {
    console.log('button2 clicked')
  }

  render() {
    const { onButton1Click, onButton2Click } = this.props
    return (
      <section>
        <button onClick={onButton1Click}>click me1</button>
        <button onClick={onButton2Click}>click me2</button>
      </section>
    )
  }
}

export default Buttons
