import React from 'react'

import Homepage from './homepage'

class AppContainer extends React.Component {
  render () {
    return <Homepage />
  }
}

export default AppContainer
