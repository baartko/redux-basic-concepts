import React from 'react'
import PropTypes from 'prop-types'

import Buttons from '../containers/buttons'

class Homepage extends React.Component {
  render () {
    return (
      <div className='page-wrapper page--homepage'>
        homepage

        <Buttons />
      </div>
    )
  }
}

Homepage.contextTypes = {
  app: PropTypes.object
}

export default Homepage
